#!/bin/sh

# SPDX-FileCopyrightText: 2023 Kalle Koskinen <kvk@iki.fi>
#
# SPDX-License-Identifier: MIT

if [ "$1" == "$2" ]
then
    echo "Don't try to use STM32CubeMX in the project directory:"
    echo
    echo "  $(readlink -f $2)."
    echo
    echo "If needed, use the IOC-file and generate code with STM32CubeMX"
    echo "into some temporary directory."
    exit
fi

mkdir -p $2/src/stm32cubemx

cp $1/*.ioc $2/
sed -i -e 's|\.\./||g' -e s'|opt/stm32|/opt/stm32|' $2/*.ioc

cp $1/startup*.s $2/src/stm32cubemx/
cp $1/*_FLASH.ld $2/src/stm32cubemx/

rm -rf $2/src/stm32cubemx/Inc
rm -rf $2/src/stm32cubemx/Src
cp -a $1/Core/Inc $2/src/stm32cubemx/
find $2/src/stm32cubemx/ -name '*.h' -exec dos2unix -q {} \;
cp -a $1/Core/Src $2/src/stm32cubemx/
find $2/src/stm32cubemx/ -name '*.c' -exec dos2unix -q {} \;

mv $2/src/stm32cubemx/Inc/main.h $2/src/stm32cubemx/Inc/board.h
mv $2/src/stm32cubemx/Src/main.c $2/src/stm32cubemx/Src/board.c

sed -i 's/main.h/board.h/g' $2/src/stm32cubemx/Inc/*
sed -i 's/main.h/board.h/g' $2/src/stm32cubemx/Src/*

sed -i 's/main.c/board.c/g' $2/src/stm32cubemx/Inc/board.h
sed -i 's/MAIN_H/BOARD_H/g' $2/src/stm32cubemx/Inc/board.h
sed -i 's/void Error_Handler(void);/void Board_Init(void);\nvoid SystemClock_Config(void);\nvoid Error_Handler(void);/' $2/src/stm32cubemx/Inc/board.h

sed -i 's/main.c/board.c/g' $2/src/stm32cubemx/Src/board.c
sed -i 's/Main program body/Board setup/g' $2/src/stm32cubemx/Src/board.c
sed -i 's/The application entry point/Board setup/g' $2/src/stm32cubemx/Src/board.c
sed -i 's/retval int/retval none/g' $2/src/stm32cubemx/Src/board.c
sed -i 's/int main(void)/void Board_Init(void)/g' $2/src/stm32cubemx/Src/board.c

sed -i '/* Infinite loop */,/* USER CODE END 3 */d' $2/src/stm32cubemx/Src/board.c

tail -n+13 $1/Makefile | head -n-1 > $2/Makefile.template
dos2unix -q $2/Makefile.template
sed -i 's/[[:blank:]]*$//' $2/Makefile.template
sed -i '/^$/N;/^\n$/D' $2/Makefile.template
sed -i 's/target/project/g' $2/Makefile.template
sed -i 's/TARGET/PROJECT/g' $2/Makefile.template
sed -i 's/main.c/board.c/g' $2/Makefile.template
sed -i 's|C_SOURCES =  |C_SOURCES = \\\nsrc/main.c |g' $2/Makefile.template
sed -i 's|Core/Src|src/stm32cubemx/Src|g' $2/Makefile.template
sed -i 's|Core/Inc|src/stm32cubemx/Inc|g' $2/Makefile.template
sed -i 's|startup_|src/stm32cubemx/startup_|g' $2/Makefile.template
sed -i 's|LDSCRIPT = |LDSCRIPT = src/stm32cubemx/|g' $2/Makefile.template
sed -i 's|all:|.PHONY: all\nall:|g' $2/Makefile.template
sed -i 's|clean:|.PHONY: clean\nclean:|g' $2/Makefile.template

sed -i '6 i #######################################' $2/Makefile.template
sed -i '7 i # target' $2/Makefile.template
sed -i '8 i #######################################' $2/Makefile.template
sed -i '9 i OPENOCD_BOARD = st_nucleo_l5' $2/Makefile.template
sed -i '9G' $2/Makefile.template

cat << EOF >> $2/Makefile.template
#######################################
# program target using OpenOCD
#######################################
.PHONY: openocd
openocd:
	openocd -f board/\${OPENOCD_BOARD}.cfg \\
		-c "program \$(BUILD_DIR)/\$(PROJECT).bin \\
		    preverify verify reset exit 0x08000000"

#######################################
# program target using st-flash
#######################################
.PHONY: st-flash
st-flash:
	st-flash write \$(BUILD_DIR)/\$(PROJECT).bin 0x08000000
	st-flash reset
EOF

cat << EOF > $2/src/main.c.template
#include "board.h"

int main(void)
{
    Board_Init();

    while (1) {
    }
}
EOF
